resource "openstack_compute_keypair_v2" "ssh-key" {
  name       = var.ssh_key_name
  public_key = var.public_key
}

resource "openstack_networking_port_v2" "port" {
  name           = var.port_name
  network_id     = var.network_id
  admin_state_up = true
  fixed_ip {
    subnet_id = var.subnet_id
  }
}

resource "openstack_compute_instance_v2" "instance" {
  name            = var.instance_name
  key_pair        = openstack_compute_keypair_v2.ssh-key.name
  flavor_name     = var.flavor_name
  image_name      = var.image_name
  security_groups = var.security_groups
  network {
    port = openstack_networking_port_v2.port.id
  }
  metadata = {
    machine_type = var.machine_type
  }
}

resource "openstack_compute_floatingip_associate_v2" "fip_associate" {
  count       = var.fip_associate == true ? 1 : 0
  floating_ip = var.floating_ip_address
  instance_id = openstack_compute_instance_v2.instance.id
  fixed_ip    = openstack_compute_instance_v2.instance.access_ip_v4
}