variable "ssh_key_name" {
  type = string
}
variable "public_key" {
  type = string
}
variable "port_name" {
  type = string
}
variable "network_id" {
  type = string
}
variable "subnet_id" {
  type = string
}
variable "instance_name" {
  type = string
}
variable "image_name" {
  type = string
}
variable "flavor_name" {
  type = string
}
variable "machine_type" {
  type = string
}
variable "security_groups" {
  type = list(string)
}
variable "fip_associate" {
  type = bool
}
variable "floating_ip_address" {
  type = string
}